'''
This is a textual representation of all Informations crawled bfrom haw landshut > faculty informatics > about us (see link below)
please make sure you are using de_DE UTF-8 and not GB ...
using gb throws ascii error in line 17 because 'ae' in the german alphabet is not existing.. (sounds strange and makes totally no sense)
author: Daniel Hilpoltsteiner 
'''

from bs4 import BeautifulSoup
import urllib, json
r = urllib.urlopen('https://www.haw-landshut.de/hochschule/fakultaeten/informatik/wir-ueber-uns/professoren-innen.html').read()
soup = BeautifulSoup(r)
#print soup

names = soup.find_all("h3")
total_length = names.__len__()
for name in names:
   print name.get_text().strip()
rooms = soup.find_all("div", class_="ui_text_long")

for room in rooms:
    string = room.p.get_text()
    roomname = string.split("\n")[0]
    print roomname.strip()
    print roomname.strip().split(":")
result = []
UUID = "5f5e2c466d1c4a9782e61b6d9368da0c"
MAJOR = 0
MINOR= 0

for x in xrange(total_length):
    # This is magic: trust me!
    room =  rooms[x].p.get_text().split("\n")[0].encode('ascii', errors='ignore').strip()
    try:
        if room.__len__() > 1:
            element = {}
            element["name"] = names[x].get_text().strip()
            roomInfo = rooms[x].p.get_text().split("\n")[0].strip()
            element["room"] = room.split(':')[1]
            element["room_no"] = room.split(':')[1][-3:]
            element["beacon"] = element["beacon"] = "{0}-{1}-{2}".format(UUID, MAJOR, element["room_no"])

        MINOR +=1
        result.append(element)
    except:
        pass

try:
    sortedResult = sorted(result, key=lambda entry: float(entry["room_no"]))
    result.sort(key= lambda entry : float(entry["room_no"]))
except:
    print('could not sort')
    pass
print json.dumps(result)
print json.dumps(sortedResult)
roomdata = open('../roomdata_scraped2.json', 'w+')
roomdata.write(json.dumps(result))
