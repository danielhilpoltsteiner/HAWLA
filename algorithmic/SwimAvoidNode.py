import turtle
import random

def isInScreen(w,t):
    leftBound = w.window_width() / -2.0
    rightBound = w.window_width() / 2.0
    bottomBound = w.window_height() / -2.0
    topBound = w.window_height() / 2.0

    turtlex = t.xcor()
    turtley = t.ycor()

    if turtlex < leftBound or turtlex > rightBound or turtley < bottomBound or turtley > topBound:
        return False

    return True

def randomWalk(t,w):
    counter = 0

    while counter < 12:
        if not isInScreen(w, t):
            t.left(180)
            counter += 1
        else:
            coin = random.randrange(0, 2)
            if coin == 0:
                turnLeft(t)
            else:
                turnRight(30)

def goForward(t):
    t.forward(50)

def turnLeft(t):
    t.left(30)
def turnRight(t):
    t.right(30)
wn = turtle.Screen()
wn.bgcolor('lightcyan')

steklovata = turtle.Turtle()
steklovata.color('darkslategray')
steklovata.shape('turtle')

randomWalk(steklovata,wn)

wn.exitonclick()