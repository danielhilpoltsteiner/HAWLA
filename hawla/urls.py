from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView, RedirectView
from hawla import views

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name="home.html"), name='home'),

    
    url(r'^roomdata', views.roomdata, name='roomdata'),
    url(r'^routine', views.routine, name='routine')
]
