#!/usr/bin/env python

import pyuarm

# init uArm: search port and connect
uarm = pyuarm.uArm()

# list port to connected uArm
# remove list [] and unicode syntax u'...'
print "Hardware Port by PyuArm Tools:"
print str(pyuarm.tools.list_uarms.uarm_ports())[3:-2]

# get_firmware_version() returns always "None"
# print uarm.get_firmware_version()
# using member variable instead
if hasattr(uarm, 'firmware_version'):
    print "Firmware Version by Member Variable: " + uarm.firmware_version
else:
    print "Firmware Version not available"

if hasattr(uarm, 'hardware_version'):
    print "Hardware Version by Member Variable: " + uarm.hardware_version
else:
    print "Hardware Version not available"

# TODO: move actions (rotate, pump)

# close connection to uArm
uarm.disconnect()