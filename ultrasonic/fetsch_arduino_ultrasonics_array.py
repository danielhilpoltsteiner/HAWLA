import serial
import time

from collections import deque

ser = serial.Serial('/dev/ttyACM0',9600)



serial_ultrasonicValues_FrontLeft  = deque([])
serial_ultrasonicValues_FrontRight = deque([])
serial_ultrasonicValues_SideLeft   = deque([])
serial_ultrasonicValues_SideRight  = deque([])

amountFetchedValues = 10


def establishConnection():

   #while True:

     # avoiding noise on channel
     time.sleep(2)

     # Trigger Arduino, with special Sensoric-Code for getting Sensorvalues
     print('Pulling for UltrasonicValues: -START-')
     #ser.write('9')
     # print "9 mal leuchten..."
     # test = int(ser.readline())
     # print test
     # print "watchdog"

     # IMPORTANT TO PRINT RESULT!!! -->
     #print ser.readline() #Got Data from Arduino?

     for x in range(0, amountFetchedValues):

       print "We're on time %d" % (x)
       ser.write('9')
       print ser.readline()  # Got Data from Arduino?
       callUltrasonicValuesFromArduino()

     ser.write('0')
     # IMPORTANT TO PRINT RESULT!!! -->
     print ser.readline()  # Got Data from Arduino?
     medianDistances = calculateMedians()


     return medianDistances


def callUltrasonicValuesFromArduino():


  serial_ultrasonicValues_FrontLeft.append(int(ser.readline()[:-2]))
  serial_ultrasonicValues_FrontRight.append(int(ser.readline()[:-2]))
  serial_ultrasonicValues_SideLeft.append(int(ser.readline()[:-2]))
  serial_ultrasonicValues_SideRight.append(int(ser.readline()[:-2]))

  print('UltrasonicValues: -START-')
  print serial_ultrasonicValues_FrontLeft
  print serial_ultrasonicValues_FrontRight
  print serial_ultrasonicValues_SideLeft
  print serial_ultrasonicValues_SideRight
  print('UltrasonicValues: -STOP-')

       # establishConnection() #Tigger Arduino again


def calculateMedians():


  print('Do Median-Calculations')

  #After Finishing 10 Loops:
  # Calculate (Sum-, &) Median-Values
  sum_FL = 0.0
  for x_FL in range(0, amountFetchedValues):
     sum_FL += float(serial_ultrasonicValues_FrontLeft[x_FL])
    # print sum_FL
  median_FL = float(sum_FL / amountFetchedValues)
  print 'Median FL:', median_FL

  sum_FR = 0.0
  for x_FR in range(0, amountFetchedValues):
      sum_FR += float(serial_ultrasonicValues_FrontRight[x_FR])
      # print sum_FR
  median_FR = float(sum_FR / amountFetchedValues)
  print'Median FR:', median_FR

  sum_SL = 0.0
  for x_SL in range(0, amountFetchedValues):
     sum_SL += float(serial_ultrasonicValues_SideLeft[x_SL])
    # print sum_SL
  median_SL = float(sum_SL / amountFetchedValues)
  print'Median SL:', median_SL

  sum_SR = 0.0
  for x_SR in range(0, amountFetchedValues):
    sum_SR += float(serial_ultrasonicValues_SideRight[x_SR])
    # print sum_SR
  median_SR = float(sum_SR / amountFetchedValues)
  print'Median SR:', median_SR


  # avoiding noise on channel
  #time.sleep(2)

  # Trigger Arduino, with special Sensoric-Code for getting Sensorvalues
  print('End Pulling for UltrasonicValues: -START-')
  #time.sleep(1)


  #print('Pulling for UltrasonicValues: -STOP-')

  medians = [median_FL, median_FR, median_SL, median_SR]
  return (medians)



