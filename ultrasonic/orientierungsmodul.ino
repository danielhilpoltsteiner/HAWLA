
#include <NewPing.h>


#define MAX_DISTANCE 200
//int maximumRange = 200; // Maximum range needed
//int minimumRange = 0; // Minimum range needed

#define sensor_FrontLeft_ECHO_PIN  2  // (Send) Echo Pin
#define sensor_FrontRight_ECHO_PIN 3  // (Send) Echo Pin
#define sensor_SideLeft_ECHO_PIN   4  // (Send) Echo Pin
#define sensor_SideRight_ECHO_PIN  5  // (Send) Echo Pin


#define sensor_FrontLeft_TRIG_PIN  8  // (Recieve) Trigger Pin
#define sensor_FrontRight_TRIG_PIN 9  // (Recieve) Trigger Pin
#define sensor_SideLeft_TRIG_PIN   10 // (Recieve) Trigger Pin
#define sensor_SideRight_TRIG_PIN  11 // (Recieve) Trigger Pin


NewPing sonar_FrontLeft(sensor_FrontLeft_TRIG_PIN,  sensor_FrontLeft_ECHO_PIN,  MAX_DISTANCE);
NewPing sonar_FrontRight(sensor_FrontRight_TRIG_PIN, sensor_FrontRight_ECHO_PIN, MAX_DISTANCE);
NewPing sonar_SideLeft(sensor_SideLeft_TRIG_PIN,   sensor_SideLeft_ECHO_PIN,   MAX_DISTANCE);
NewPing sonar_SideRight(sensor_SideRight_TRIG_PIN,  sensor_SideRight_ECHO_PIN,  MAX_DISTANCE);


int sensor_FrontLeft_distanceCm;
int sensor_FrontRight_distanceCm;
int sensor_SideLeft_distanceCm;
int sensor_SideRight_distanceCm;


const int ledPin = 13;

void setup() {
 Serial.begin (9600);



pinMode(ledPin, OUTPUT);

}

void loop() {

//  Calibrate Distances:
//  getAllDistances();


    readMasterTrigger();
//    getAllDistancesOnBlock();


}


void readMasterTrigger(){
//  int triggercode = 1234;

//Serial.print(1234);
  if (Serial.available()) {
    // Catching Trigger from Arduino-Ultrasonic-Sonde:

    //Serial.write("GOT VALUES");
     //test(Serial.read()-'0');
     startSending(Serial.read()-'0');
  }
  delay(500);

}

void test(int n) {

  Serial.print(n);
  for (int i = 0; i < n; i++) {
    Serial.println("Light LED!");
    digitalWrite(ledPin, HIGH);
    delay(500);
    digitalWrite(ledPin, LOW);
    delay(500);
  }
}

//Waiting for Triggercode (9) from Master-RaspberryPi, to start transfering SensorData
void startSending(int n) {

  // printDistance(n);
  delay(100);
  if(n == 9) {
    Serial.println("Getting data from Arduino");
    getAllDistancesOnBlock();
  }

  else if (n == 0) {
    Serial.println("End getting data from Arduino");
  }
  else {
    Serial.println("Not getting data from Arduino");
  }

}


// ### Print Distances: ###
void getAllDistances() {
  Serial.println("All Distances:");
  Serial.println("----------------------------------");

  Serial.print("Ping sonar_FrontLeft: ");
  delay(100);  // Wartet 100 Milisekunden zwischen den Pings (ca. 10 Pings pro Sekunde). 29 Millisekunden ist der kürzest mögliche Delay zwischen zwei Pings.
  sensor_FrontLeft_distanceCm = sonar_FrontLeft.ping_cm();
  printDistance( sensor_FrontLeft_distanceCm );

  Serial.print("Ping sonar_FrontRight: ");
  delay(100);  // Wartet 100 Milisekunden zwischen den Pings (ca. 10 Pings pro Sekunde). 29 Millisekunden ist der kürzest mögliche Delay zwischen zwei Pings.
  sensor_FrontRight_distanceCm = sonar_FrontRight.ping_cm();
  printDistance( sensor_FrontRight_distanceCm );

  Serial.print("Ping sonar_SideLeft: ");
  delay(100);  // Wartet 100 Milisekunden zwischen den Pings (ca. 10 Pings pro Sekunde). 29 Millisekunden ist der kürzest mögliche Delay zwischen zwei Pings.
  sensor_SideLeft_distanceCm = sonar_SideLeft.ping_cm();
  printDistance( sensor_SideLeft_distanceCm );

  Serial.print("Ping sonar_SideRight: ");
  delay(100);  // Wartet 100 Milisekunden zwischen den Pings (ca. 10 Pings pro Sekunde). 29 Millisekunden ist der kürzest mögliche Delay zwischen zwei Pings.
  sensor_SideRight_distanceCm = sonar_SideRight.ping_cm();
  printDistance( sensor_SideRight_distanceCm );

  delay(500);
}


void getAllDistancesOnBlock() {

  delay(100);  // Wartet 100 Milisekunden zwischen den Pings (ca. 10 Pings pro Sekunde). 29 Millisekunden ist der kürzest mögliche Delay zwischen zwei Pings.
  sensor_FrontLeft_distanceCm = sonar_FrontLeft.ping_cm();
  delay(100);  // Wartet 100 Milisekunden zwischen den Pings (ca. 10 Pings pro Sekunde). 29 Millisekunden ist der kürzest mögliche Delay zwischen zwei Pings.
  sensor_FrontRight_distanceCm = sonar_FrontRight.ping_cm();
  delay(100);  // Wartet 100 Milisekunden zwischen den Pings (ca. 10 Pings pro Sekunde). 29 Millisekunden ist der kürzest mögliche Delay zwischen zwei Pings.
  sensor_SideLeft_distanceCm = sonar_SideLeft.ping_cm();
  delay(100);  // Wartet 100 Milisekunden zwischen den Pings (ca. 10 Pings pro Sekunde). 29 Millisekunden ist der kürzest mögliche Delay zwischen zwei Pings.
  sensor_SideRight_distanceCm = sonar_SideRight.ping_cm();

  printDistance( sensor_FrontLeft_distanceCm );
  printDistance( sensor_FrontRight_distanceCm );
  printDistance( sensor_SideLeft_distanceCm );
  printDistance( sensor_SideRight_distanceCm );

  delay(1000);                  // give the loop some break
}


void printDistance( int p_distanceCm) {

  Serial.println(p_distanceCm);
  // Serial.println(" cm");

}
